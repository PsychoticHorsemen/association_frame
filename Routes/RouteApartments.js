/////   REQUIRE     ///////////////////////////////////////////////////////////////////////////////////////////////////
var express = require("express");
var Apartments = require("../Apartments");
var request = require("request");
var router = express.Router();

/////   END POINTS  ///////////////////////////////////////////////////////////////////////////////////////////////////
//GET {info.address, _id} for all apartments on association server
router.get("/apartments", function(req, res){
    Apartments.find({},{'info.address':1}, function(err, apartments){
        if(err){
            console.log(err);
        }
        else{
            res.json(apartments);
        }
    });
});

//GET all {apartmentSchema} for a specific apartment on association server
router.get("/apartments/:ida", function(req, res){
    var ida = req.params.ida;
    Apartments.find({'_id':ida}, function(err, apartment){
        if(err){
            console.log(err);
        }
        else{
            res.json(apartment);
        }
    });
});

//GET all {sensor_id} for a specific apartment on association server
router.get("/apartments/:ida/sensors", function(req, res){
    var ida = req.params.ida;
    Apartments.find({'_id':ida},{'sensors':1}, function(err, apartment){
        if(err){
            console.log(err);
        }
        else{
            res.json(apartment);
        }
    });
});

//GET all {sensor_id, timestamp, data} for a specific apartment on association server
router.get("/apartments/:ida/data", function(req, res){
    var ida = req.params.ida;
    Apartments.find({'_id':ida},{'sensors':1}, function(err, apartment){
        if(err){
            console.log(err);
        }
        else{
            var selectedData = [];
            apartment[0].sensors.forEach(function(value){
                var tmp = JSON.stringify(value);
                tmp = JSON.parse(tmp);

                selectedData.push({"sensorInfo": tmp.sensorInfo, "data": tmp.data });
            });

            res.json(selectedData);
        }
    });
});

//GET all {sensor_id, timestamp, data} for a specific sensor type in specific apartment on association server
router.get("/apartments/:ida/data/:type", function(req, res){
    var ida = req.params.ida;
    var type = req.params.type;
    Apartments.find({'_id':ida},{'sensors':1}, function(req1, res1){
        var sensors = res1;
        var selectedSensors = [];

        sensors[0].sensors.forEach(function(value){
            var info = JSON.parse(JSON.stringify(value));

            if(info.sensorInfo.type == type){
                selectedSensors.push(info);
            }
        });
        res.json(selectedSensors);
    });
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Developer End-Point
router.get("/apartments/:ida/dev_connect", function (req, res) {
    var ida = req.params.ida;

    Apartments.findOne({"_id": ida}, {"tcp_address": 1, "sensors": 1}, function (rq, doc) {

        var ip = doc.tcp_address.ip;
        var port = doc.tcp_address.port;
        var sensors = doc.sensors;

        httpGet = {
            url: "http://"+ip+":"+port+"/data/current",
            json: true
        };

        request(httpGet, function (error, response, body) {
            if(error) res.send(error);
            else {

                body.forEach(function (data) {

                    sensors.forEach(function (sensor) {

                        if(sensor.sensorID === data.sensorId) {

                            data.data.forEach(function (value) {

                                sensor.data.push(value);

                            });
                        }

                    });

                });

                Apartments.findOneAndUpdate({ "_id": ida }, {
                    $set: {
                        sensors: sensors
                    }
                }, function (error, doc) {

                    if(error) res.json(error);

                    res.json(doc);

                });
            }
        });
    })

});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EXPORT
module.exports = router;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////