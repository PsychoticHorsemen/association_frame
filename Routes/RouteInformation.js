/////   REQUIRE     ///////////////////////////////////////////////////////////////////////////////////////////////////
var express = require("express");
var Apartments = require("../Apartments");
var router = express.Router();

/////   END POINTS  ///////////////////////////////////////////////////////////////////////////////////////////////////
// Information based end-points
router.get("/apartments/:ida/info", function (req, res) {
    var ida = req.params.ida;

    Apartments.find({"_id": ida}, {"info": 1, "tcp_address": 1, "occupants": 1}, function (rq, rs) {
        res.json(rs);
    })

});

router.get("/apartments/:ida/info/info", function (req, res) {
    var ida = req.params.ida;

    Apartments.find({"_id": ida}, {"info": 1}, function (rq, rs) {
        res.json(rs);
    })

});

router.get("/apartments/:ida/info/tcp", function (req, res) {
    var ida = req.params.ida;

    Apartments.find({"_id": ida}, {"tcp_address": 1}, function (rq, rs) {
        res.json(rs);
    })

});

router.get("/apartments/:ida/info/occupants", function (req, res) {
    var ida = req.params.ida;

    Apartments.find({"_id": ida}, {"occupants": 1}, function (rq, rs) {
        res.json(rs);
    })

});

router.get("/db_size", function (req, res) {

    Apartments.collection.stats({ scale: 1024 }, function (error, result) {

        var stats = {
            type: "kilobytes (kb)",
            avg_document_size: result.avgObjSize,
            total_documents_size: result.size,
            total_for_all_indexes: result.totalIndexSize,
            storage_size: result.storageSize
        };

        res.json(stats);
    });

});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EXPORT
module.exports = router;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////