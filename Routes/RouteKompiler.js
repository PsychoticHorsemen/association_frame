/////   REQUIRE     ///////////////////////////////////////////////////////////////////////////////////////////////////
var express = require("express");
var router = express.Router();
var kompiler = require("../Controllers/KompilerApp");

/////   END POINTS  ///////////////////////////////////////////////////////////////////////////////////////////////////
// Kompiler
router.get("/all", function (req, res) {

    kompiler.compileAllData().then(function (result) {

        res.json(result);

    }).catch(function (error) {

        res.status(500).send("Error compiling data.\n"+ error);

    });

});


router.get("/all/:type", function (req, res) {

    var type = req.params.type;

    kompiler.compileAllDataForType(type).then(function (result) {

        res.json(result);

    }).catch(function (error) {

        res.status(500).send("Error compiling data for: " + type + "\n"+ error);

    });

});


router.get("/id/:id", function (req, res) {

    var id = req.params.id;

    kompiler.compileAllDataForID(id).then(function (result) {

        res.json(result);

    }).catch(function (error) {

        res.status(500).send("Error compiling data for: " + id + "\n"+ error);

    });

});

router.get("/selected", function (req, res) {

    var ids = req.query.list;

    kompiler.compileSelectedIDs(ids).then(function (result) {
        res.json(result);
    }).catch(function (error) {
        res.status(500).send("Error compiling: " + ids + "\n" + error);
    });
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EXPORT
module.exports = router;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////