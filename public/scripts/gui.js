// this is for controlling the gui.
// gui calls logic.js and buttonFunctions when it wants to do anything.
// logic.js should not call gui. (only using callbacks)

$(function () {

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var url = "http://localhost:8080/";

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // search bar
    var searchBarInput = $("#searchBarInput");

    // footer buttons
    var searchButton = $("#searchButton");
    var transferButton = $("#transferButton");
    var compileSettingsButton = $("#compileSettingsButton");
    var dataTypesButton = $("#dataTypesButton");
    var constraintsButton = $("#constraintsButton");

    // add remove buttons
    var addButton = $("#addButton");
    var removeButton = $("#removeButton");

    // Buttons status effects
    var buttonOK = {backgroundColor: "darkseagreen", borderColor: "#74a274"};
    var buttonWarning = {backgroundColor: "indianred", borderColor: "#ac4d4d"};
    var buttonUnavailable = {backgroundColor: "grey", borderColor: "dimgrey"};

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var leftTree = $('#left_tree');
    var rightTree = $('#right_tree');
    var loriumForening = $("#loriumForening");
    var dataTypes = $("#dataTypesList");

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $("#addAllButton").on("click", function (event) {
        event.preventDefault();

        addAllFromTab();
    });

    $("#removeAllButton").on("click", function (event) {
        event.preventDefault();

        removeAllFromTab();
    });

    var onclick_statistics = function () {
        $(".accordion h3:eq(1)").on("click", function () {

            displayStatistik();
        });
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // populate the left tree with fresh data from the database
    PopulateDataLeftTree();
    function PopulateDataLeftTree() {
        leftTree.find(".accordion").accordion({
            collapsible: true
        });

        leftTree.find(".accordion").find(".accordion").accordion( "option", "active", 2 );

        // get lejligheder
        $.get("/apartments", function(data) {

            var list = document.createElement("ol");
            list.id = "selectable";

            data.map(function (item) {

                var id = item._id;
                var address = item.info.address;

                // console.log(item);

                var lejlighed = document.createElement("li");
                lejlighed.id = id;
                lejlighed.className = "ui-widget-content";
                lejlighed.innerHTML = address;

                list.append(lejlighed);

            });

            loriumForening.append(list);

            $("#selectable").selectable();

            onclick_statistics();
        })
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    searchButton.click(function () {
        console.log("Search button clicked.");
    });

    transferButton.click(function () {
        console.log("Transfer button clicked.");
    });

    compileSettingsButton.click(function () {

        kompileSelectedData();
    });

    dataTypesButton.click(function () {
        console.log("Data types button clicked.");
    });

    constraintsButton.click(function () {
        console.log("Constraints button clicked.");
    });

    addButton.click(function () {

        var selected = $("#selectable .ui-selected");

        for(var i = 0; i<selected.length; i++) {

            var item = selected[i];


            addToRightTree(item);

        }
    });

    removeButton.click(function () {
        var selected = $("#selectable_rt .ui-selected");

        for(var i = 0; i<selected.length; i++) {

            var item = selected[i];

            $("#selectable_rt #"+item.id).remove();
        }

        if($("#selectable_rt").text() === "") {
            rightTree.find(".accordion").remove();
            dataTypes.empty();
        }
        displayStatistik();
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var addToRightTree = function (selected) {

        if(rightTree.text() === "") {

            createNewRightTree("Denmark", "LoriumIpsum Boligforening");

            $("#selectable_rt").selectable({
                selecting: function( event, ui ) { displayInformation(ui) }
            });
        }

        var rt = $("#selectable_rt");

        var id = selected.id;
        var address = selected.innerHTML;

        var lejlighed = document.createElement("li");
        lejlighed.id = id;
        lejlighed.className = "ui-widget-content";
        lejlighed.innerHTML = address;

        addDataTypeToSelection(id);

        rt.append(lejlighed);
    };

    var displayInformation = function (ui) {

        var selected = $(ui.selecting);
        var id = selected.attr("id");
        var address = selected.text();

        console.log("ID: " + id + ", Address: " + address);

        $.get("./templates/information.hbs").then(function (template) {

            var compiled = Handlebars.compile(template);

            $.getJSON("apartments/" + id).then(function (data) {

                var html = compiled(data[0]);

                $(".selectedInfoWrap").html(html);
            });

        });
    };

    var displayStatistik = function () {
        $.get("./templates/statistik.hbs").then(function (template) {

            var compiled = Handlebars.compile(template);

            $.getJSON("db_size").then(function (data) {

                var html = compiled(data);

                $(".selectedInfoWrap").html(html);
            });

        });
    };

    var addDataTypeToSelection = function (id) {

        $.getJSON("/apartments/" + id + "/sensors").then(function (sensors) {

            var data = sensors[0].sensors;

            for(var i = 0; i<data.length; i++) {

                var type = data[i].sensorInfo.type;

                var exists = !(dataTypes.find("#"+type).length === 0);


                if(!exists) {

                    var li = document.createElement("li");
                    li.id = type;
                    li.innerHTML = type;
                    li.onclick = selectType;
                    $(li).addClass("type");
                    $(li).addClass("type-active");

                    dataTypes.append(li);

                }
                currentTypes.addItem(type);

            }

            console.log(currentTypes);

        });
    };

    var kompileSelectedData = function(el) {

        var rt = $("#selectable_rt");
        var typesList = $("#dataTypesList");

        /// Get list of selected data
        var seletedData = rt.find("li");
        var ids = [];

        for(var i = 0; i<seletedData.length; i++) {
            ids.push(seletedData[i].id);
        }

        /// Get list of active types
        var selectedTypes = typesList.find(".type-active");
        var dataTypes = [];

        for(var i = 0; i<selectedTypes.length; i++) {
            dataTypes.push(selectedTypes[i].id);
        }

        /// Get checkbox anon
        var checkbox_anon = $("#data_is_anon:checked").val();
        var anon = false;

        if(checkbox_anon === "on") {
            anon = true;
        }

        $.get("/kompiler/selected", {
            "list[]": ids,
            "types[]": dataTypes,
            "anon": anon
        }, function (data) {

            console.log(data);
            downloadData(data);
        });

    };

    var kompileSelectedApartment = function (id) {

        /// Get list of active types
        var typesList = $("#dataTypesList");
        var selectedTypes = typesList.find(".type-active");
        var dataTypes = [];

        for(var i = 0; i<selectedTypes.length; i++) {
            dataTypes.push(selectedTypes[i].id);
        }

        /// Get checkbox anon
        var checkbox_anon = $("#data_is_anon:checked").val();
        var anon = false;

        if(checkbox_anon === "on") {
            anon = true;
        }

        $.get("/kompiler/"+id, {
            "types[]": dataTypes,
            "anon": anon
        }, function (data) {

            console.log(data);
            downloadData(data);
        });
    };


    var downloadData = function (data) {
        $("<a />", {
            "download": "Compiled Data.json",
            "href" : "data:application/json," + encodeURIComponent(JSON.stringify(data,0,4))
        }).appendTo("body")
            .click(function() {
                $(this).remove()
            })[0].click()
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var addAllFromTab = function () {
        var tab = leftTree.find(".accordion").find(".accordion").accordion( "option", "active" );
        var selected = leftTree.find(".accordion").find(".accordion div").eq(tab);

        var list = selected.find("ol");
        var data = list.find("li");


        for(var i = 0; i<data.length; i++) {

            var item = data[i];

            addToRightTree(item);

        }
    };

    var removeAllFromTab = function () {
        var tab = rightTree.find(".accordion").find(".accordion").accordion( "option", "active" );
        var selected = rightTree.find(".accordion").find(".accordion div").eq(tab);

        var list = selected.find("ol");
        var data = list.find("li");


        for(var i = 0; i<data.length; i++) {

            var item = data[i];

            item.remove();

        }

        rightTree.find(".accordion").remove();
        dataTypes.empty();
    };


    var createNewRightTree = function (root_parent, parent) {

        var div = document.createElement("div");
        div.className = "accordion";

        var h3 = document.createElement("h3");
        h3.innerHTML = root_parent;

        var div_content = document.createElement("div");
        div_content.className = "accordion";

        var h3_content = document.createElement("h3");
        h3_content.innerHTML = parent;

        var div_list_content = document.createElement("div");
        div_list_content.id = "rightTree_lorium";
        div_list_content.className = "accordion";

        div_content.append(h3_content);
        div_content.append(div_list_content);
        div.append(h3);
        div.append(div_content);

        rightTree.append(div);


        rightTree.find(".accordion").accordion({
            collapsible: true
        });

        var list = document.createElement("ol");
        list.id = "selectable_rt";

        div_list_content.append(list);

        onclick_statistics();
    };

    var currentTypes = {

        addItem: function (item) {
            if(currentTypes[item] == undefined) {
                currentTypes[item] = 1;
            } else {
                currentTypes[item]++;
            }
        },

        removeItem: function (item) {
            if(currentTypes[item] != undefined) {
                currentTypes[item]--;

                if(currentTypes[item] === 0) {
                    dataTypes.find(item).remove();
                }

            }
        }
    };

    var selectType = function () {

        var me = $(this);

        if(me.hasClass("type-active")) {

            me.removeClass("type-active");
            me.addClass("type-inactive");

        } else {

            me.removeClass("type-inactive");
            me.addClass("type-active");

        }

    };

/////   INITIALIZE      ////////////////////////////////////////////////////////////////////////////////////////////

    transferButton.css("background-color", buttonUnavailable.backgroundColor);
    transferButton.css("border-color", buttonUnavailable.borderColor);

    compileSettingsButton.css("background-color", buttonWarning.backgroundColor);
    compileSettingsButton.css("border-color", buttonWarning.borderColor);

    dataTypesButton.css("background-color", buttonWarning.backgroundColor);
    dataTypesButton.css("border-color", buttonWarning.borderColor);
});




