//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var url = "http://localhost:8080/";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(function () {

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i');
        } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i');
        }
        e.stopPropagation();
    });

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $.getJSON(url+"apartments").then(function (data) {

        data.reverse();

        data.forEach(function (value) {

            console.log(value);

            var list_item = document.createElement("li");
            list_item.className = "apartment_child";
            list_item.id = value._id;
            list_item.innerHTML = "<span>" + value.info.address + "</span>";

            list_item.onclick = getInformationPage;

            $("#apartment_list").append(list_item);
        });
    });

    function consolelogme() {
        var selected = $(this);
        console.log(selected.attr("id"));
    }

    function getInformationPage() {
        var selected = $(this);

        var id = selected.attr("id");

        console.log(id);

        $.get("./templates/information.hbs").then(function (template) {

            var compiled = Handlebars.compile(template);

            $.getJSON(url+"apartments/"+id).then(function (data) {

                console.log(data[0]);

                var html = compiled(data[0]);

                $("#informationRow").html(html);
            });

        });
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

});