var mongoose = require("mongoose");
var should = require("should");
var kompiler = require("../Controllers/KompilerApp");
var Apartments = require("../Apartments");

var dbURI = "mongodb://root:toor@ds155299.mlab.com:55299/association_database";
// var dbURI = "mongodb://localhost:27017/association_database";

var TEST_ID = undefined;

before("Opret Test lejlighed", function (done) {

    mongoose.connect(dbURI, function () {

        var apartment = new Apartments({
            info: {
                size:       "100",
                rooms:      "1",
                address:    "_ACCEPT_TEST_"
            },
            tcp_address: {
                ip:         "127.0.0.1",
                port:       "8080"
            },
            occupants:      [{
                name: "Test-Manson",
                date: "2018-05-01"
            }],
            sensors:        [{
                sensorID: "Sensor_01",
                sensorInfo: {
                    type: "Temperature",
                    date: "2018-05-01",
                    placement: "1"
                },
                data: [
                    {
                        timestamp: "2018-05-01 10:00:00",
                        data: "25"
                    },
                    {
                        timestamp: "2018-05-01 10:01:00",
                        data: "27"
                    },
                    {
                        timestamp: "2018-05-01 10:02:00",
                        data: "22"
                    }]
            },
                {
                    sensorID: "Sensor_02",
                    sensorInfo: {
                        type: "Noise",
                        date: "2018-05-01",
                        placement: "2"
                    },
                    data: [
                        {
                            timestamp: "2018-05-01 10:00:00",
                            data: "50"
                        },
                        {
                            timestamp: "2018-05-01 10:01:00",
                            data: "72"
                        },
                        {
                            timestamp: "2018-05-01 10:02:00",
                            data: "65"
                        }]
                }]
        });



        apartment.save().then(function () {
            TEST_ID = apartment._id;
            console.log("Test ID: " + TEST_ID);
            console.log("Created Test-data...");
        }).catch(function (error) {
            throw error;
        });


        done();
    });

});

after("Delete Test-lejlighed", function () {
    Apartments.remove({"_id": TEST_ID}, function (error) {
        if(error) {
            throw error;
        }
    });
});

/// Accept Tests for Test-ID 4
/// User-Story omkring kompilering af dataset

// Test for acceptkriterie 1
describe("Kompiler alt data for lejligheder", function () {

    // TODO: Improve test
    it("compileAllData returner et array af JSON", function () {

        return kompiler.compileAllData().then(function (json) {

            // Der skal vaere minimum 1 lejlighed
            json.length.should.be.greaterThanOrEqual(1);

            var info = json[0].info;
            (typeof info.address).should.be.equal("undefined"); // INGEN PERSONLIG INFORMATION!
            (typeof json.occupants).should.be.equal("undefined");

            // Vi har minimum 1 sensor.
            json[0].sensors.length.should.be.greaterThanOrEqual(1);

            // Vi har minimum 1 datapunkt.
            json[0].sensors[0].data.length.should.be.greaterThanOrEqual(1);

            return true;
        });

    });

});

// Test for acceptkriterie 2
describe("Kompiler alt data for en lejligheder", function () {

    // TODO: Improve test
    it("compileAllDataForID returner et array af JSON", function () {

        return kompiler.compileAllDataForID(TEST_ID).then(function (json) {
            // Der skal vaere minimum 1 lejlighed
            json.length.should.be.greaterThanOrEqual(1);

            var info = json[0].info;
            (typeof info.address).should.be.equal("undefined"); // INGEN PERSONLIG INFORMATION!
            (typeof json.occupants).should.be.equal("undefined");

            // Vi har minimum 1 sensor.
            json[0].sensors.length.should.be.greaterThanOrEqual(1);

            // Vi har minimum 1 datapunkt.
            json[0].sensors[0].data.length.should.be.greaterThanOrEqual(1);

            return true;
        });

    });

});


// Test for acceptkriterie 3
describe("Kompiler alt data for specifikt data type for alle lejligheder", function () {

    // TODO: Improve Test
    it("compileAllDataForType returner et array af JSON kun med sensor data", function () {

        return kompiler.compileAllDataForType("Temperature").then(function (json) {
            // Der skal vaere minimum 1 lejlighed
            json.length.should.be.greaterThanOrEqual(1);

            var info = json[0].info;
            (typeof info).should.be.equal("undefined"); // INGEN PERSONLIG INFORMATION!
            (typeof json.occupants).should.be.equal("undefined");

            return true;
        });

    });

});


///// FORKASTET FUNKTIONALITET
// // Test for acceptkriterie 4
// describe("Kompiler alt data for en periode", function () {
//
//     // TODO: Re-evaluate test and improve
//     it("compileAllDataForPeriod returner et array af JSON", function () {
//
//         return kompiler.compileAllDataForPeriod("2018-05-01", "2018-05-3").then(function (json) {
//             // Der skal vaere 3 ud af 4 lejligheder
//             json.length.should.be.equal(3);
//
//             var info = json[0].info;
//             (typeof info.address).should.be.equal("undefined"); // INGEN PERSONLIG INFORMATION!
//             (typeof json.occupants).should.be.equal("undefined");
//
//             // Vi har minimum 1 sensor.
//             json[0].sensors.length.should.be.greaterThanOrEqual(1);
//
//             // Vi har minimum 1 datapunkt.
//             json[0].sensors[0].data.length.should.be.greaterThanOrEqual(1);
//
//             return true;
//         });
//     });
//
// });

// Test for acceptkriterie 5
describe("Kompiler alt data for udvalgte lejligehder", function () {

    // TODO: Improve Test
    it("compileSelectedIDs returner et array af JSON kun med sensor data", function () {

        return kompiler.compileSelectedIDs([TEST_ID]).then(function (json) {
            // Der skal vaere minimum 1 lejlighed
            json.length.should.be.greaterThanOrEqual(1);
            (typeof json.occupants).should.be.equal("undefined");

            return true;
        });

    });

});

// FORKASTET FUNKTIONALITET
// // Test for acceptkriterie 6
// describe("Kompiler alt data for udvalgte lejligehder og data typer", function () {
//
//     // TODO: Improve Test
//     it("compileSelectedIDs returner et array af JSON kun med sensor data", function () {
//
//         return kompiler.compileSelectedIDsAndTypes([TEST_ID], ["Noise"]).then(function (json) {
//             // Der skal vaere minimum 1 lejlighed
//             json.length.should.be.greaterThanOrEqual(1);
//
//             var info = json[0].info;
//
//             info.toString().should.be.equal("undefined"); // INGEN PERSONLIG INFORMATION!
//             (typeof json.occupants).should.be.equal("undefined");
//
//             json[0].sensors.length.should.be.equal(1);
//
//             return true;
//         });
//
//     });
//
// });