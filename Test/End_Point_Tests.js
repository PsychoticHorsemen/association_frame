var mongoose = require("mongoose");
var request = require("supertest");
var should = require("should");
var app = require("../app");
var Apartments = require("../Apartments");

var dbURI = "mongodb://root:toor@ds155299.mlab.com:55299/association_database";
// var dbURI = "mongodb://localhost:27017/association_database";
var TEST_ID = undefined;

before("Opret Test lejlighed", function (done) {

    mongoose.connect(dbURI, function () {

        var apartment = new Apartments({
            info: {
                size: "100",
                rooms: "1",
                address: "_ACCEPT_TEST_"
            },
            tcp_address: {
                ip: "127.0.0.1",
                port: "8080"
            },
            occupants: [{
                name: "Test-Manson",
                date: "2018-05-01"
            }],
            sensors: [{
                sensorID: "Sensor_01",
                sensorInfo: {
                    type: "Temperature",
                    date: "2018-05-01",
                    placement: "1"
                },
                data: [
                    {
                        timestamp: "2018-05-01 10:00:00",
                        data: "25"
                    },
                    {
                        timestamp: "2018-05-01 10:01:00",
                        data: "27"
                    },
                    {
                        timestamp: "2018-05-01 10:02:00",
                        data: "22"
                    }]
            }]
        });

        apartment.save().then(function () {
            TEST_ID = apartment._id;
            console.log("Test ID: " + TEST_ID);
            console.log("Created Test-data...");
            done();
        }).catch(function (error) {
            throw error;
        });
    });

});

after("Delete Test-lejlighed", function () {
    Apartments.remove({"_id": TEST_ID}, function (error) {
        if(error) {
            throw error;
        } else {
            console.log("Remove test data...");
        }
    });
});

/// Accept Tests for Test-ID 1 (User-Story 1) og 2 (User-Story 21)
/// User-Story omkring Datahandling og Estimering af data stoerrelse

// Test for acceptkriterie 4 fra User-Story 1 og acceptkriterie 1 fra User-Story 21
describe("Hent alle lejligheder test og faa stoerrelse af database", function () {

    it("get('/apartments') - alle lejligheder test, skal gerne have minimum 1 lejlighed", function () {
        return request(app)
            .get("/apartments")
            .expect(200)
            .expect("Content-Type", /json/)
            .then(function (docs) {
                var res = docs.body;
                res.length.should.be.greaterThanOrEqual(1);

                var exists = false;
                res.forEach(function (value) {
                    if(value._id === TEST_ID.toString()) {
                        exists = true;
                    }
                });
                exists.should.be.true();
            });
    });

    it("Faa stoerrelse af database, skal vaere stoerre end 0", function () {

        return request(app)
            .get("/db_size")
            .expect(200)
            .expect("Content-Type", /json/)
            .then(function (docs) {
                var res = docs.body;

                res.total_documents_size.should.be.greaterThan(1);
                res.total_for_all_indexes.should.be.greaterThan(1);
                res.storage_size.should.be.greaterThanOrEqual(40);
            });

    });

});

// Test for acceptkriterie 5 og 6 fra User-Story 21
describe("Hent en leje", function () {

    it("get('apartments/:ida - hent specifik data fra 1 lejlighed'", function () {
        return request(app)
            .get("/apartments/"+TEST_ID)
            .expect(200)
            .expect("Content-Type", /json/)
            .then(function (docs) {
                var res = docs.body;

                res[res.length-1]._id.should.be.equal(TEST_ID.toString()); // Anvender toString() fordi variablen er Object
            });
    });

    it("get('apartments/:ida/sensors' - hent sensorer fra lejlighed", function () {
        return request(app)
            .get("/apartments/" + TEST_ID + "/sensors")
            .expect(200)
            .expect("Content-Type", /json/)
            .then(function (docs) {
                var res = docs.body;

                res.length.should.be.greaterThan(0);
                res[0].sensors[0].sensorID.should.be.equal("Sensor_01");
                res[0].sensors[0].data.length.should.be.equal(3);
                res[0].sensors[0].sensorInfo.type.should.be.equal("Temperature");
            });
    });

    it("get('apartments/:ida/data' - hent ALT data fra lejlighed", function () {
        return request(app)
            .get("/apartments/" + TEST_ID + "/data")
            .expect(200)
            .expect("Content-Type", /json/)
            .then(function (docs) {
                var res = docs.body;

                res.length.should.be.greaterThanOrEqual(1);
                res[0].data.length.should.be.equal(3);
                res[0].sensorInfo.type.should.be.equal("Temperature");
            });
    });

    it("get('apartments/:ida/data/Temperature' - hent specifik type af data", function () {
        return request(app)
            .get("/apartments/" + TEST_ID + "/data/Temperature")
            .expect(200)
            .expect("Content-Type", /json/)
            .then(function (docs) {
                var res = docs.body;

                res.length.should.be.greaterThanOrEqual(1);
                res[0].sensorID.should.be.equal("Sensor_01");
                res[0].data.length.should.be.equal(3);
                res[0].data[0].data.should.be.equal("25");
                res[0].data[0].timestamp.should.be.equal("2018-05-01 10:00:00");
            });
    });

});