var Apartments = require("../Apartments");
var mongoose = require("mongoose");

exports.compileAllData = function () {

    return new Promise(((resolve, reject) => {

        Apartments.find({}, {"sensors": 1, "_id": 0}, function (error, doc) {
            if(error) reject();


            resolve(doc);
        });

    }));
};

exports.compileAllDataForType = function (type) {
    return new Promise(((resolve, reject) => {

        Apartments.find({}, {"sensors": 1, "_id": 0}, function (error, documents) {
            if(error) reject();

            var selectedSensors = [];

            documents.forEach(function (doc) {
                doc.sensors.forEach(function(value){
                    var info = JSON.parse(JSON.stringify(value));

                    console.log(value);

                    if(info.sensorInfo.type == type){
                        selectedSensors.push(info);
                    }
                });
            });


            resolve(selectedSensors);
        });

    }));
};

exports.compileAllDataForID = function (ida) {

    return new Promise(((resolve, reject) => {

        Apartments.find({"_id": ida}, {"sensors": 1, "_id": 0}, function (error, doc) {
            if(error) reject();


            resolve(doc);
        });

    }));

};

exports.compileSelectedIDs = function(ids) {
    return new Promise(((resolve, reject) => {

        var objids = [];

        ids.forEach(function (val) {

            objids.push(mongoose.Types.ObjectId(val))

        });

        // console.log(objids);

        Apartments.find({"_id": { $in: objids }}, { "occupants": 0 }, function (error, doc) {
            if(error) reject();


            resolve(doc);
        });

    }));
};

exports.compileSelectedIDsAndTypes = function(ids, types) {
    return new Promise(((resolve, reject) => {

        var objids = [];

        ids.forEach(function (val) {

            objids.push(mongoose.Types.ObjectId(val))

        });

        console.log(types);

        // Apartments.aggregate([
        //
        //     {
        //         $match: {
        //             $in: objids
        //         }
        //     },
        //     {
        //         $project: {
        //             sensors: {
        //                 $filter: {
        //                     input: "$sensors",
        //                     as: "sensors",
        //                     cond: { $in: [
        //                         "$$sensorInfo.type",
        //                             types
        //                         ]
        //                     }
        //                 }
        //             }
        //         }
        //     }
        //
        //     ], function (error, documents) {
        //     if(error) reject();
        //
        //     console.log(documents);
        //
        //     resolve(documents);
        // });

        Apartments.find({"_id": { $in: objids }, "sensors.sensorInfo.type": { $in: types }}, { "info": 0 }, function (error, documents) {
            if(error) reject();

            console.log(documents);

            resolve(documents);
        });

    }));
};

///// FORKASTET FUNKTIONALITET
// exports.compileAllDataForPeriod = function (startDato, slutDato) {
//     return new Promise(((resolve, reject) => {
//
//         Apartments.find({}, {"sensors": 1, "_id": 0}, function (error, documents) {
//             if(error) reject();
//
//             var selectedSensors = [];
//
//             documents[0].sensors.forEach(function(value){
//                 var info = JSON.parse(JSON.stringify(value));
//
//                 if(info.sensorInfo.type == type){
//                     selectedSensors.push(info);
//                 }
//             });
//
//             resolve(selectedSensors);
//         });
//
//     }));
// };