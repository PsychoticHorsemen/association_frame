var Apartments = require("../Apartments");
var request = require("request");

var dataLoop = function (arg) {

    var promise = new Promise(((resolve, reject) => {
        Apartments.find({}, { "tcp_address": 1, "sensors": 1 }, function (error, result) {

            if (error) {
                reject(error);
            } else {
                resolve(result);
            }

        });
    }));

    promise
        .then((data) => {

            data.forEach(function (value) {

                setTimeout(function () {

                    pushDataToDatabase(value)

                }, 1000);

            });
        })
        .catch("error", function (error) {
            console.log(error);
        });

    setTimeout(dataLoop, 10000);
};


/////   PUSH DATA   ///////////////////////////////////////////////////////////////////////////////////////////////////

var pushDataToDatabase = function (doc) {

    var ida = doc._id;
    var ip = doc.tcp_address.ip;
    var port = doc.tcp_address.port;
    var sensors = doc.sensors;


    var httpGet = {
        url: "http://" + ip + ":" + port + "/data/current",
        json: true
    };

    request(httpGet, function (error, response, body) {
        if (error) {
            if(error.code === "ECONNREFUSED") {
                console.log("Could not connect to: " + error.address + " - for [" + ida + "]");
            }
            return error;
        }
        else {


            body.forEach(function (data) {

                sensors.forEach(function (sensor) {

                    if (sensor.sensorID === data.sensorId) {


                        data.data.forEach(function (value) {

                            sensor.data.push(value);

                        });
                    }

                });

            });


            Apartments.update({"_id": ida}, {
                $set: {
                    sensors: sensors
                }
            }, function (error) {
                if (error) return error;
                console.log("Updated: " + ida);
            });
        }
    });
};

module.exports = dataLoop;
