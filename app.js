/////   REQUIRE     ///////////////////////////////////////////////////////////////////////////////////////////////////
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Apartments = require("./Apartments");
/////   DEFINE     ////////////////////////////////////////////////////////////////////////////////////////////////////
var updater = require("./Controllers/DatabaseUpdater");
var port = process.env.PORT || 8080;
var app = express();
mongoose.Promise = global.Promise;
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));

/////   DATABASE CONNECTION   /////////////////////////////////////////////////////////////////////////////////////////
var dbURI = "mongodb://root:toor@ds155299.mlab.com:55299/association_database";
// var dbURI = "mongodb://localhost:27017/association_database";
mongoose.connect(dbURI, function () {
    console.log("Connected to database");

    updater();
});


/////   ROUTES      ///////////////////////////////////////////////////////////////////////////////////////////////////
var apartment_routes = require("./Routes/RouteApartments");
var information_routes = require("./Routes/RouteInformation");
var kompiler_routes = require("./Routes/RouteKompiler");
app.use("/", apartment_routes);
app.use("/", information_routes);
app.use("/kompiler", kompiler_routes);


/////   RUN     ///////////////////////////////////////////////////////////////////////////////////////////////////////
app.listen(port);
module.exports = app;
console.log("Listening on port: " + port);
console.log("Listening on routes:\n" +
    "localhost:8080/apartments\n" +
    "localhost:8080/apartments/:id\n" +
    "localhost:8080/apartments/:id/sensors\n" +
    "localhost:8080/apartments/:id/data\n" +
    "localhost:8080/apartments/:ida/data/:type\n" +
    "*****************************************\n");